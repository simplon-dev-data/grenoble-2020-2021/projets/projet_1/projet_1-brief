![Cover](cover.png)

# Mesure d'audience sur le web

> Analyser les logs d'un serveur web d'un site-web e-commerce afin d'obtenir une mesure d'audience dans le temps

## Contexte du projet

Nous avons récupéré les logs d'un serveur d'un site-web de e-commerce. Nous souhaitons pouvoir effectuer une mesure d'audience des visiteurs.

Les logs du serveur web sont au format "Common Log Format" qui semble être communément utilisé sur le web.

Nous nous posons dans un premier temps les questions suivantes :

- Quel est le traffic sur le site ?
- Combien de personnes visitent le site ?
- Quels sont nos produits les plus et moins intéressants pour notre clientèle ?
- De quel endroit d'internet viennent les visiteurs avant d'arriver sur le site ?
- Quels moments de la journée sont les plus/moins propices à la vente ?

Par ailleurs, il paraitrait qu'il est possible de savoir :

- Quels navigateurs sont les plus / moins utilisés par les visiteurs ?
- Quels systèmes d'exploitation sont les plus / moins utilisés par les visiteurs ?
- Quels appareils sont les plus / moins utilisés par les visiteurs ?

Et nous avons vu chez des concurrents qu'il était possible d'obtenir :

- D'où sur la planète viennent les visiteurs du site-web ?

Schéma fonctionnel général :

![Schema fonctionnel](web-analytics-schema.png)

## Ressource(s)

- [Logs du serveur web](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/3QBYB5)
- [Base de données GeoIP](https://dev.maxmind.com/geoip/geoip2/geolite2/)
- [Wikipedia - Common Log Format](https://en.wikipedia.org/wiki/Common_Log_Format)

## Livrables

- Dépôt GitLab
- Code source fonctionnel (Bash, Python, SQL)
- Documents de réflexion et de conception
- L'outil de suivi d'avancement du projet

## Modalités pédagogiques

Le projet est à réaliser en binôme sur 1 semaine. Au moins 1 point d'avancement par groupe sera fait au cours de la semaine.

## Critères de performance

Technologies : Git, Bash, SQL, SQLite, Python, Metabase

## Modalités d'évaluation

La restitution des résultats se fera la semaine suivante par groupe :

- 15mn max de presentation
- 15mn max de débrief

La présentation devra exposer votre analyse des problèmes et les solutions développées pour y répondre. Egalement, un premier retour d'expérience sur le travail collaboratif est attendu.

## Compétences visées

- C1. Concevoir et structurer physiquement une base de données relationnelle ou non, à partir des besoins, contraintes et données du commanditaire : **niveau 1**

- C2. Acquérir des données, les combiner et les structurer en données propres en vue de leur intégration dans la structure de la base de données : **niveau 2**

- C3. Intégrer des données propres et préparées dans la base de données finale, en utilisant des langages informatiques, logiciels ou outils : **niveau 2**

- C5. Interroger la base de données afin de mettre à jour les données (brutes ou traitées) stockées, provisoirement ou durablement, en fonction du résultat recherché : **niveau 1**

- C6. Concevoir et réaliser un rendu visuel des données issues du processus d'extraction, à l’aide d’un (des) support(s) adapté(s) répondant aux attentes du commanditaire : **niveau 2**

- C8. Analyser et formaliser la demande ou le besoin en développement et en exploitation de base de données : **niveau 1**

- C9. Autocontrôler, tout au long du processus de développement, la cohérence des données et la conformité à la demande : **niveau 1**

- C10. Suivre, adapter et rendre compte de la réalisation du projet à partir du planning projet validé : **niveau 1**

- C11. Rechercher des solutions pour la résolution de problèmes techniques rencontrés au moyen des ressources disponibles (documentation, sites Internet, communautés, etc..) : **niveau 2**
